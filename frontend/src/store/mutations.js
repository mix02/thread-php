import { SET_LOADING } from './mutationTypes';

export default {
    [SET_LOADING]: (state, isLoading = true) => {
        console.log('Common mutations ', isLoading);
        state.isLoading = isLoading;
    },
};
