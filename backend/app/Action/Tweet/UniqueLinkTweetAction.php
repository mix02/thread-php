<?php

declare(strict_types=1);

namespace App\Action\Tweet;

// use App\Models\Like;

use App\Models\UniqueLink;
use App\Repository\UniqueLinkRepository;
use App\Repository\TweetRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

final class UniqueLinkTweetAction
{
    public function __construct(
        // private TweetRepository $tweetRepository,
        private UniqueLinkRepository $uniqueLinkRepository,
    ) {
    }

    public function execute(UniqueLinkTweetRequest $request): void
    {
        // $tweet = $this->tweetRepository->getById($request->getId());

        $uniqueLink = new UniqueLink();
    
        $uniqueLink->link = $request->getUrl();
        $uniqueLink->code = Str::random(30);

        // $uniqueLink->forTweet(Auth::id(), $tweet->id);

        $this->likeRepository->save($uniqueLink);

        // return new LikeTweetResponse(self::ADD_LIKE_STATUS);
    }
}
