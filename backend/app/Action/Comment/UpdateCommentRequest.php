<?php

declare(strict_types=1);

namespace App\Action\Comment;

final class UpdateCommentRequest
{
    public function __construct(private int $id, private ?string $body)
    {
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getBody(): ?string
    {
        return $this->body;
    }
}
