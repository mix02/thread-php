<?php

declare(strict_types=1);

namespace App\Http\Request\Api\Tweet;

use App\Http\Request\ApiFormRequest;

final class CheckTweetHttpRequest extends ApiFormRequest
{
    public function rules(): array
    {
        return [
            'link' => 'required|url'
        ];
    }
}
