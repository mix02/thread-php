<?php

declare (strict_types = 1);

namespace App\Http\Controllers\Api;


use App\Action\Tweet\UniqueLinkTweetAction;
use App\Action\Tweet\UniqueLinkTweetRequest;
use App\Http\Controllers\ApiController;
use App\Http\Request\Api\Tweet\CheckTweetHttpRequest;
use App\Http\Response\ApiResponse;

final class UniqueLinkController extends ApiController 
{
    public function makeUniqueLink(
        CheckTweetHttpRequest $request,
        UniqueLinkTweetAction $action,
        string $id
    ): ApiResponse {
        $response = $action->execute(new UniqueLinkTweetRequest((int) $id, $request->get('link')));

        // return $this->createSuccessResponse(['status' => $response->getStatus()]);

        return ApiResponse::empty();
    }

    
    
}
